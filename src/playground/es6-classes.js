class Person {
  constructor(name = 'Anonymous', age = 0) {
    this.name = name;
    this.age = age;
  }

  getGreeting() {
    return `Hi. I am ${this.name}!`;
  }

  getDescription() {
    return `${this.name} is ${this.age} year(s) old.`;
  }
}

class Traveler extends Person {
  constructor(name, age, homeLocation) {
    super(name, age);
    this.homeLocation = homeLocation;
  }

  hasHomeLocation() {
    return !!this.homeLocation;
  }

  getGreeting() {
    if (this.hasHomeLocation()) {
      return `${super.getGreeting()} I'm visiting from ${this.homeLocation}.`;
    }
    return super.getGreeting();
  }
}

const person1 = new Traveler('Uziel Bueno', 34, 'Cuernavaca, Mor.');
const person2 = new Traveler();
console.log(person1.getGreeting());
console.log(person2.getGreeting());
