console.log('App.js is running.');

const appRoot = document.getElementById('app');
// JSX - Javascript XML

const app = {
  title: 'Indecision App',
  subtitle: 'Put your life in the hands of a computer.',
  items: [
    { title: 'Painting the house.' },
    { title: 'Learn React' },
    { title: 'Master Docker and Kubernetes' }
  ]
};

const onSubmitForm = e => {
  e.preventDefault();
  const option = e.target.elements.option.value;

  if (option) {
    app.items.push({ title: option });
    e.target.elements.option.value = '';
    renderApp();
  }
};

const removeItems = e => {
  app.items = [];
  renderApp();
};

const onMakeDecision = () => {
  const randomNum = Math.floor(Math.random() * app.items.length);
  const option = app.items[randomNum];
  alert(option.title);
};

const renderApp = () => {
  const template = (
    <div>
      <h1 id="yay!">{app.title}</h1>
      {app.subtitle && <p>{app.subtitle}</p>}
      <p>{app.items.length > 0 ? 'Here are your options:' : 'No options.'}</p>
      <button disabled={app.items.length === 0} onClick={onMakeDecision}>
        What should I do?
      </button>
      <button onClick={removeItems}>Remove All</button>
      <ol>
        {app.items.map((item, key) => (
          <li key={key}>{item.title}</li>
        ))}
      </ol>

      <form onSubmit={onSubmitForm}>
        <input type="text" name="option" />
        <button>Add Option</button>
      </form>
    </div>
  );

  ReactDOM.render(template, appRoot);
};

renderApp();
