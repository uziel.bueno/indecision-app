class VisibilityToggle extends React.Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = {
      visible: false
    };
  }

  handleToggle() {
    this.setState(prevState => {
      return {
        visible: !prevState.visible
      };
    });
  }

  render() {
    return (
      <div>
        <h1>Visibility Toggle</h1>
        <button onClick={this.handleToggle}>
          {this.state.visible ? 'Hide details' : 'Show details'}
        </button>
        {this.state.visible && <p>Details are here. You clicked the button!</p>}
      </div>
    );
  }
}

class ToggleButton extends React.Component {
  render() {
    return (
      <button>{this.props.visible ? 'Hide details' : 'Show details'}</button>
    );
  }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById('app'));
// let visible = false;

// const onShowDetails = () => {
//   visible = !visible;
//   render();
// };

// const render = () => {
//   const template = (
//     <div>
//       <h1>Visibility Toggle</h1>
//       <button onClick={onShowDetails}>
//         {visible ? 'Hide details' : 'Show details'}
//       </button>
//       {visible && <p>Details are here. You clicked the button!</p>}
//     </div>
//   );

//   ReactDOM.render(template, document.getElementById('app'));
// };

// render();
