const multiplier = {
  numbers: [1, 2, 3],
  multiplyBy: 2,
  multilply() {
    // -> ES6 method definition syntax.
    return this.numbers.map(number => number * this.multiplyBy);
  }
};

console.log(multiplier.multilply());
