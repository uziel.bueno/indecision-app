class IndecisionApp extends React.Component {
  constructor(props) {
    super(props);
    this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
    this.handlePickOption = this.handlePickOption.bind(this);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.handleDeleteOption = this.handleDeleteOption.bind(this);
    this.state = {
      options: props.options
    };
  }

  componentDidMount() {
    console.log('componentDidMount');
    try {
      // In case the json parsing throws an error...
      const json = localStorage.getItem('options');
      const options = JSON.parse(json);

      if (options) {
        this.setState(() => ({ options }));
      }
    } catch (error) {
      // Do nothing at all...
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem('options', json);
    }
    console.log('componentDidUpdate');
  }
  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  handleDeleteOptions() {
    this.setState(() => ({
      options: []
    }));
  }

  handleDeleteOption(option) {
    this.setState(prevState => ({
      options: prevState.options.filter(item => item !== option)
    }));
  }

  handlePickOption() {
    const number = Math.floor(Math.random() * this.state.options.length);
    const option = this.state.options[number];
    alert(option);
  }

  handleAddOption(option) {
    if (!option) {
      return 'Enter valid value to add an option';
    }

    if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists';
    }

    this.setState(prevState => ({
      options: [...prevState.options, option]
    }));
  }

  render() {
    const title = 'Indecision';
    const subtitle = 'Put your life in the hands of a computer';

    return (
      <div>
        <Header subtitle={subtitle} />
        <Action
          onPickOption={this.handlePickOption}
          hasOptions={this.state.options.length > 0}
        />
        <Options
          options={this.state.options}
          onDeleteOptions={this.handleDeleteOptions}
          handleDeleteOption={this.handleDeleteOption}
        />
        <AddOption onAddOption={this.handleAddOption} />
      </div>
    );
  }
}

IndecisionApp.defaultProps = {
  options: []
};

const Header = props => {
  return (
    <div>
      <h1>{props.title}</h1>
      {props.subtitle && <h2>{props.subtitle}</h2>}
    </div>
  );
};

Header.defaultProps = {
  title: 'Indecision'
};

// class Header extends React.Component {
//   render() {
//     return (
//       <div>
//         <h1>{this.props.title}</h1>
//         <h2>{this.props.subtitle}</h2>
//       </div>
//     );
//   }
// }

const Action = props => {
  return (
    <div>
      <button onClick={props.onPickOption} disabled={!props.hasOptions}>
        What should I do?
      </button>
    </div>
  );
};

// class Action extends React.Component {
//   render() {
//     return (
//       <div>
//         <button
//           onClick={this.props.onPickOption}
//           disabled={!this.props.hasOptions}
//         >
//           What should I do?
//         </button>
//       </div>
//     );
//   }
// }

const Options = props => {
  return (
    <div>
      <button onClick={props.onDeleteOptions}>Remove all</button>
      {props.options.length === 0 && (
        <p>Please add an option to get started.</p>
      )}
      <ul>
        {props.options.map((item, key) => (
          <Option
            key={key}
            option={item}
            handleDeleteOption={props.handleDeleteOption}
          />
        ))}
      </ul>
    </div>
  );
};

// class Options extends React.Component {
//   render() {
//     return (
//       <div>
//         <button onClick={this.props.onDeleteOptions}>Remove all</button>
//         <ul>
//           {this.props.options.map((item, key) => (
//             <Option key={key} option={item} />
//           ))}
//         </ul>
//       </div>
//     );
//   }
// }

const Option = props => {
  return (
    <li>
      {props.option}
      <button
        onClick={e => {
          e.preventDefault();
          props.handleDeleteOption(props.option);
        }}
      >
        remove
      </button>
    </li>
  );
};

// class Option extends React.Component {
//   render() {
//     return <li>{this.props.option}</li>;
//   }
// }

class AddOption extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.state = {
      error: undefined
    };
  }

  handleAddOption(event) {
    event.preventDefault();

    const option = event.target.elements.option.value.trim();
    const error = this.props.onAddOption(option);
    this.setState(() => ({
      error: error
    }));

    if (!error) {
      event.target.reset();
    }
  }

  render() {
    return (
      <div>
        {this.state.error && <p>{this.state.error}</p>}
        <form onSubmit={this.handleAddOption}>
          <input type="text" name="option" />
          <button type="submit">Add</button>
        </form>
      </div>
    );
  }
}

ReactDOM.render(<IndecisionApp options={[]} />, document.getElementById('app'));
